
You can find the starting code at `the gitlab repository am4ip-lab2 <https://gitlab.com/am4ip/am4ip-lab2>`_

Exercise 1 - Implementation of VAE
==================================

The am4ip library is composed of the following modules:

::

    am4ip
    │   datasets.py
    │   losses.py
    │   metrics.py
    │   models.py
    │   trainer.py
    │   utils.py
    │   utils.py

The role of `datasets.py` is to prepare the data. This includes data loading and pre-processing.

`losses.py` contains loss function that are computed to train a VAE.

`metrics.py` which will contain Inception Score and Frechet Inception Distance
to evaluate generative models.

`models.py` will contain all implemented models, as well as a code skeleton for VAE models.

`trainer.py` contains a utility function to perform the training.

Finally, `utils.py` contains a set of tools and utility function that are helpful, but not mandatory to used.

Activity: Extend the python script `train.py` in the script folder (or start with a notebook from scratch) as follows:

`train.py` is a skeleton of the code, including data loading into batches, the overall training procedure, etc.
Your goal is to implement different parts of this training procedure, including the model, the loss, the sampling, the visualization, etc.

1. Load LFW dataset
2. Build a variational auto-encoder
3. Compute the loss as a reconstruction loss (MSE) and KL divergence
4. Perform the training
5. Compute IS and FID scores
6. Show some generated images

Exercise 2 - Beta-VAE
=====================
Beta-VAE enforces by a factor beta the KL divergence between the estimated mean and standard deviation
of the learned probability density function, and the expected distribution (Gaussian with 0 mean and unitary covariance).

1. Update the computation of the loss function
2. Retrain the network
3. Compute IS and FID scores
4. Show some generated images

Exercise 3 - Conditional (beta) VAE
===================================
Add branches to both the decoder and the encoder which will represent attributes of the input data.
The goal of conditioning the VAE with this strategy is to enforce additional structure in the
latent space.

1. Add an additional input and an additional output to the decoder and encoder
2. Update the loss function to handle these attributes
3. Re-train the network
4. Compute IS and FID scores
5. Show some generated images
