.. am4ip

Advanced Methods for Image Processing - Lab 2
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Generative Models

   exercises
   api
