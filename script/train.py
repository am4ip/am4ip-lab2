
import torch
from torchvision.transforms import Compose, PILToTensor
from torch.utils.data import DataLoader

from am4ip.dataset import LFWAttributes
from am4ip.models import VariationalAutoEncoder
from am4ip.trainer import BaselineTrainer
from am4ip.losses import VAELoss
from am4ip.metrics import InceptionScore, FrechetInceptionDistance

transform = Compose([lambda z: z.resize((64, 64)),  # Image of size 64x64 for faster training
                     PILToTensor(),
                     lambda z: z.to(dtype=torch.float32) / 127.5 - 1  # Normalize between -1 and 1
                     ])
batch_size = 32
lr = 1e-3
epoch = 1

# Load data and generate batches
train_dataset = LFWAttributes(mode="train", transform=transform)
train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)

val_dataset = LFWAttributes(mode="val", transform=transform)
val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False)

# Implement VAE model:
# TODO: complete parameters and implement model forward pass + sampling
model = VariationalAutoEncoder()

# Implement loss function:
# TODO: implement the loss function as presented in the course
loss = VAELoss()

# Choose optimize:
optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.9)

# Implement the trainer
trainer = BaselineTrainer(model=model, loss=loss, optimizer=optimizer)

# Do the training
trainer.fit(train_loader, epoch=epoch)

# Compute metrics
# TODO: implement metrics
metrics = [InceptionScore(),
           FrechetInceptionDistance()]

trainer.eval(val_loader, metrics)



print("job's done.")
