
import torch
from torch.nn import Module


class VariationalAutoEncoder(Module):
    def __init__(self, latent_dim: int,
                 encoder_module: Module,
                 decoder_module: Module):
        super().__init__()
        self.latent_dim = latent_dim
        self.encoder_module = encoder_module
        self.decoder_module = decoder_module

    def sample(self, mean: torch.Tensor, log_std: torch.Tensor):
        """

        :param mean: :math:`\mu(x)`: the mean obtained from x. Size = N x  latent_dim
        :param log_std: :math:`\Sigma(x)`: the diagonal terms for the standard deviation, estimated as :math:`\log \sigma_i^2`. Size = N x  latent_dim.
        :return: Samples with Gaussian distribution :math:`\mathcal{N}(\mu(x), \Sigma(x)`
        """
        raise NotImplementedError

    def forward(self, x: torch.Tensor):
        """Forward pass for the network.

        :return: A torch.Tensor.
        """
        raise NotImplementedError
