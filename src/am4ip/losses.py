
import torch
# noinspection PyProtectedMember
from torch.nn.modules.loss import _Loss


class VAELoss(_Loss):
    def __init__(self):
        super(VAELoss, self).__init__()

    def forward(self, inp: torch.Tensor, target: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError
