
import torch
from typing import List
from torch.utils.data import DataLoader
from abc import ABC, abstractmethod


class Metric(ABC):
    """Abstract metric class to evaluate generative models.
    """
    def __init__(self, name: str, use_cuda: bool = True) -> None:
        self.name = name
        self.use_cuda = use_cuda

    def compute_metric(self, data_loader: DataLoader, model: torch.nn.Module) -> torch.Tensor:
        cumulative_score = 0.
        n_batch = 0.
        with torch.no_grad():
            for i, (x, attr) in enumerate(data_loader):
                # Move data to cuda is necessary:
                if self.use_cuda:
                    x = x.cuda()
                    attr = attr.cuda()

                out = self.batch_compute([x, attr], model)
                cumulative_score += out.sum(dim=0)
                n_batch += 1.

            res = out / n_batch

        return res

    @abstractmethod
    def batch_compute(self, inp: List[torch.Tensor], model: torch.nn.Module):
        raise NotImplementedError


class InceptionScore(Metric):
    # TODO implementation of InceptionScore
    def __init__(self, use_cuda: bool = True):
        super().__init__("Inception Score", use_cuda=use_cuda)

    def batch_compute(self, inp: List[torch.Tensor], model: torch.nn.Module):
        raise NotImplementedError


class FrechetInceptionDistance(Metric):
    # TODO implementation of Frechet Inception Distance
    def __init__(self, use_cuda: bool = True):
        super().__init__("Frechet Inception Distance", use_cuda=use_cuda)

    def batch_compute(self, inp: List[torch.Tensor], model: torch.nn.Module):
        raise NotImplementedError
