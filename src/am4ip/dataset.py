
import os
import numpy as np
from glob import glob
from PIL import Image
from numpy.typing import NDArray
from torch.utils.data import Dataset
from typing import Optional, Callable, Tuple

from am4ip.utils import expanded_join


class LFWAttributes(Dataset):
    """Class utility to load, pre-process, put in batch, and convert to PyTorch convention images from the LFW-Attributes dataset.
    """

    # root_path = "C:\\RESOURCES\\datasets\\lfw-a"
    root_path = "/net/ens/am4ip/datasets/lfw-a"

    def __init__(self, transform: Optional[Callable] = None,
                 target_transform: Optional[Callable] = None,
                 mode="train") -> None:
        """ Class initialization.

        :param transform: A set of transformations to apply on data.
        :param target_transform: A set of transformations to apply on labels.
        """
        self.mode = mode
        id_list = os.listdir(expanded_join(self.root_path, "lfw"))
        person_folders = glob(expanded_join(self.root_path, "lfw/*"))
        person_folders.sort()
        stop = int(0.8 * len(person_folders))
        training_img_list = []
        validation_img_list = []

        for person_folder in person_folders[:stop]:
            training_img_list.append(glob(expanded_join(person_folder, "*")))

        for person_folder in person_folders[stop:]:
            validation_img_list.append(glob(expanded_join(person_folder, "*")))

        # Get attributes and images:
        attributes = []
        person_images = []
        with open(expanded_join(self.root_path, "lfw_attributes.txt"), mode='r') as f:
            f.readline()
            header = f.readline().split("\t")[3:]  # drop two first columns (name and image number)
            for line in f:
                split_line = line.split("\t")
                name = split_line[0].replace(" ", "_")

                if name in id_list:
                    img_id = f"{name}_{int(split_line[1]):04d}.jpg"

                    attributes.append([float(s) for s in split_line[1:]])
                    person_images.append(expanded_join(self.root_path, "lfw", name, img_id))
                    if not os.path.exists(expanded_join(self.root_path, "lfw", name, img_id)):
                        print(f"Error {expanded_join(self.root_path, 'lfw', name, img_id)}")

        person_images = np.array(person_images)
        attributes = np.array(attributes)

        stop = int(0.8 * len(person_images))

        self.train_person_images = person_images[:stop]
        self.val_person_images = person_images[stop:]
        self.train_attributes = attributes[:stop]
        self.val_attributes = attributes[stop:]

        self.transform = transform
        self.target_transform = target_transform

    def __len__(self):
        """Dataset size.
        :return: Size of the dataset.
        """
        if self.mode == "train":
            return len(self.train_person_images)
        else:
            return len(self.val_person_images)

    def __getitem__(self, index:  int) -> Tuple[NDArray, NDArray]:
        if self.mode == "train":
            img = Image.open(self.train_person_images[index])
            attr = self.train_attributes[index]
        else:
            img = Image.open(self.val_person_images[index])
            attr = self.val_attributes[index]

        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            attr = self.target_transform(attr)

        return img, attr
